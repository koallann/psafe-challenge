package me.koallann.psafechallenge.workers

import android.content.Context
import androidx.work.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import me.koallann.psafechallenge.BuildConfig
import me.koallann.psafechallenge.R
import me.koallann.psafechallenge.data.currency.CurrencyRepository
import me.koallann.psafechallenge.data.currency.CurrencyRepositoryImpl
import me.koallann.psafechallenge.data_db.currency.CurrencyDaoClient
import me.koallann.psafechallenge.data_network.currency.CurrencyWebService
import java.util.*
import java.util.concurrent.TimeUnit

class CurrencyWorker(
    appContext: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams) {

    companion object {

        fun schedule(context: Context) {
            val constraints = Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()
            val request = PeriodicWorkRequestBuilder<CurrencyWorker>(15, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .build()

            WorkManager.getInstance(context).enqueueUniquePeriodicWork(
                "refresh_currencies",
                ExistingPeriodicWorkPolicy.KEEP,
                request
            )
        }

    }

    private val currencyCode: String by lazy {
        java.util.Currency.getInstance(Locale.getDefault()).currencyCode
    }
    private val repository: CurrencyRepository by lazy {
        CurrencyRepositoryImpl(
            CurrencyDaoClient(applicationContext),
            CurrencyWebService(
                applicationContext.getString(R.string.currencies_base_url),
                BuildConfig.API_KEY,
                currencyCode
            )
        )
    }

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            repository.refreshCurrencies()
            Result.success()
        } catch (e: Exception) {
            Result.failure()
        }
    }

}
