package me.koallann.psafechallenge.data_network.currency.response

import com.google.gson.annotations.SerializedName

class CurrenciesResponse(
    @SerializedName("data")
    val currencies: List<CurrencyResponse>
)
