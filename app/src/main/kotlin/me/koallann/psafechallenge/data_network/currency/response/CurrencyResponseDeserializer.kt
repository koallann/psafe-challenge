package me.koallann.psafechallenge.data_network.currency.response

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class CurrencyResponseDeserializer(val currencyCode: String) : JsonDeserializer<CurrencyResponse> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): CurrencyResponse {
        json ?: return CurrencyResponse()

        val currencyJson = json.asJsonObject
        val quoteJson = currencyJson.get("quote")?.asJsonObject?.get(currencyCode)?.asJsonObject

        val quote = CurrencyResponse.QuoteResponse(
            quoteJson?.get("market_cap")?.asDouble ?: 0.0,
            quoteJson?.get("volume_24h")?.asDouble ?: 0.0,
            quoteJson?.get("price")?.asDouble ?: 0.0,
            quoteJson?.get("percent_change_24h")?.asFloat ?: 0.0f
        )
        return CurrencyResponse(
            currencyJson.get("name")?.asString ?: "",
            currencyJson.get("symbol")?.asString ?: "",
            currencyJson.get("circulating_supply")?.asLong ?: 0L,
            quote
        )
    }
}
