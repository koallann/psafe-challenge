package me.koallann.psafechallenge.data_network

import com.google.gson.Gson
import me.koallann.psafechallenge.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object WebServiceFactory {

    fun <T> create(
        baseUrl: String,
        service: Class<T>,
        gson: Gson = Gson(),
        vararg interceptors: Interceptor
    ): T {
        val clientBuilder = getClientBuilder()
        interceptors.forEach { clientBuilder.addInterceptor(it) }

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(clientBuilder.build())
            .build()
            .create(service)
    }

    fun getClientBuilder(): OkHttpClient.Builder {
        val builder = OkHttpClient.Builder().readTimeout(1, TimeUnit.MINUTES)
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            }
            builder.addInterceptor(logging)
        }
        return builder
    }

}
