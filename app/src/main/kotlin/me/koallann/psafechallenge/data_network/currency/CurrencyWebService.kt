package me.koallann.psafechallenge.data_network.currency

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import me.koallann.psafechallenge.data.currency.CurrencyRemoteDataSource
import me.koallann.psafechallenge.data_network.WebServiceFactory
import me.koallann.psafechallenge.data_network.currency.response.CurrencyResponse
import me.koallann.psafechallenge.data_network.currency.response.CurrencyResponseDeserializer
import me.koallann.psafechallenge.domain.Currency

class CurrencyWebService(
    baseUrl: String,
    apiKey: String,
    val currencyCode: String
) : CurrencyRemoteDataSource {

    private val gson: Gson = GsonBuilder().also {
        it.registerTypeAdapter(
            CurrencyResponse::class.java,
            CurrencyResponseDeserializer(currencyCode)
        )
    }.create()

    private val restApi: CurrencyRestApi = WebServiceFactory.create(
        baseUrl,
        CurrencyRestApi::class.java,
        gson,
        AuthorizationInterceptor(apiKey)
    )

    override suspend fun getCurrencies(): List<Currency> {
        return restApi.getCurrencies(currencyCode).currencies.map {
            it.toDomain().apply { quote.iso4217Code = currencyCode }
        }
    }

}
