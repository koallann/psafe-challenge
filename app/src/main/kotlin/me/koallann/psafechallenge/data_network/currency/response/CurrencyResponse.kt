package me.koallann.psafechallenge.data_network.currency.response

import me.koallann.psafechallenge.domain.Currency

class CurrencyResponse(
    var name: String = "",
    var symbol: String = "",
    var circulatingSupply: Long = 0L,
    var quote: QuoteResponse = QuoteResponse()
) {
    fun toDomain(): Currency = Currency(name, symbol, circulatingSupply, quote.toDomain())

    class QuoteResponse(
        var marketCap: Double = 0.0,
        var volume24h: Double = 0.0,
        var price: Double = 0.0,
        var pricePercent24h: Float = 0.0f
    ) {
        fun toDomain(): Currency.Quote = Currency.Quote(
            marketCap = marketCap,
            volume24h = volume24h,
            price = price,
            pricePercent24h = pricePercent24h
        )
    }
}
