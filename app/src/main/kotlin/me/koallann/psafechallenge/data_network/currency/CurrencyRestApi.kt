package me.koallann.psafechallenge.data_network.currency

import me.koallann.psafechallenge.data_network.currency.response.CurrenciesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyRestApi {

    @GET("/v1/cryptocurrency/listings/latest?start=1&limit=10")
    suspend fun getCurrencies(@Query("convert") currencyCode: String): CurrenciesResponse

}
