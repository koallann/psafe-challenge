package me.koallann.psafechallenge.data_network.currency

import okhttp3.Interceptor
import okhttp3.Response

class AuthorizationInterceptor(private val apiKey: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder().header("X-CMC_PRO_API_KEY", apiKey)
        val request = builder.build()

        return chain.proceed(request)
    }

}
