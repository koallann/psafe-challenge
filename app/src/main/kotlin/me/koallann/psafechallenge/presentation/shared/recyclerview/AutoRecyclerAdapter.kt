package me.koallann.psafechallenge.presentation.shared.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class AutoRecyclerAdapter<E, VH : ViewHolder<E>>(
    private val onCreateViewHolder: (
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ) -> VH
) : RecyclerView.Adapter<VH>() {

    private val items: MutableList<E> by lazy { ArrayList<E>() }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val layoutInflater = LayoutInflater.from(parent.context)
        return onCreateViewHolder(layoutInflater, parent, viewType)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(items[position])
    }

    fun size(): Int {
        return items.size
    }

    fun getAll(): List<E> {
        return items
    }

    fun add(item: E) {
        synchronized(this) {
            items.add(item)
            notifyItemInserted(items.size - 1)
        }
    }

    fun addAll(items: List<E>) {
        synchronized(this) {
            val start = this.items.size
            this.items.addAll(items)
            notifyItemRangeInserted(start, items.size)
        }
    }

    fun clear() {
        synchronized(this) {
            items.clear()
            notifyDataSetChanged()
        }
    }

}
