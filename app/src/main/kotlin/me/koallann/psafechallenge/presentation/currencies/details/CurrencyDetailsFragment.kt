package me.koallann.psafechallenge.presentation.currencies.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import me.koallann.psafechallenge.R
import me.koallann.psafechallenge.databinding.FragmentCurrencyDetailsBinding
import me.koallann.psafechallenge.domain.Currency
import me.koallann.psafechallenge.presentation.shared.extensions.format

class CurrencyDetailsFragment : Fragment() {

    private val binding: FragmentCurrencyDetailsBinding by lazy {
        FragmentCurrencyDetailsBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val currency = arguments?.getParcelable<Currency>("currency")
        currency?.let { setupLayout(it) }
    }

    private fun setupLayout(currency: Currency) {
        binding.also {
            it.currency = currency

            val currencyCode = currency.quote.iso4217Code
            it.price.text = currency.quote.price.format(currencyCode)
            it.marketCap.text = currency.quote.marketCap.format(currencyCode)
            it.volume.text = currency.quote.volume24h.format(currencyCode)

            it.priceChange.text = getString(
                R.string.label_currency_details_price_change_value,
                currency.quote.pricePercent24h.format()
            )
            it.circulatingSupply.text = getString(
                R.string.label_currency_details_circulating_supply_value,
                currency.circulatingSupply.format(),
                currency.symbol
            )
        }
    }

}
