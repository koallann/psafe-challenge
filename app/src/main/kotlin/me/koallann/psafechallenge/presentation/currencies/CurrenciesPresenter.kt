package me.koallann.psafechallenge.presentation.currencies

import kotlinx.coroutines.*
import me.koallann.psafechallenge.R
import me.koallann.psafechallenge.data.currency.CurrencyRepository
import me.koallann.psafechallenge.domain.Currency
import me.koallann.psafechallenge.presentation.shared.coroutines.DispatcherProvider
import me.koallann.psafechallenge.presentation.shared.mvp.Presenter

class CurrenciesPresenter(
    private val currencyRepository: CurrencyRepository,
    private val dispatcherProvider: DispatcherProvider
) : Presenter<CurrenciesView>(CurrenciesView::class.java) {

    private val scope = CoroutineScope(dispatcherProvider.main)

    override fun detachView() {
        scope.cancel()
        super.detachView()
    }

    fun onLoadCurrencies() {
        scope.launch {
            view?.showLoading()
            try {
                val currencies = withContext(dispatcherProvider.io) {
                    currencyRepository.getCurrencies()
                }
                view?.showCurrencies(currencies)
            } catch (e: Exception) {
                view?.showMessage(R.string.msg_error_loading_currencies)
            }
            view?.dismissLoading()
        }
    }

    fun onClickCurrency(currency: Currency) {
        view?.navigateToDetails(currency)
    }

}
