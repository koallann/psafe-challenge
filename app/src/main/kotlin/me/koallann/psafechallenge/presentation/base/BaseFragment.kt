package me.koallann.psafechallenge.presentation.base

import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import me.koallann.psafechallenge.presentation.shared.mvp.BasicView

abstract class BaseFragment : Fragment(), BasicView {

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun showMessage(@StringRes messageRes: Int) {
        showMessage(getString(messageRes))
    }

}
