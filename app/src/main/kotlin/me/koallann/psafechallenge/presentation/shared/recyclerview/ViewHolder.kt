package me.koallann.psafechallenge.presentation.shared.recyclerview

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class ViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun onBind(item: T)

    fun bind(item: T) {
        onBind(item)
    }

}
