package me.koallann.psafechallenge.presentation.currencies

import me.koallann.psafechallenge.domain.Currency
import me.koallann.psafechallenge.presentation.shared.mvp.BasicView

interface CurrenciesView : BasicView {

    fun showLoading()

    fun dismissLoading()

    fun showCurrencies(currencies: List<Currency>)

    fun navigateToDetails(currency: Currency)

}
