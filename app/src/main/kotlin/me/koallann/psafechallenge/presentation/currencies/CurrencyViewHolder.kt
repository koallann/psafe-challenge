package me.koallann.psafechallenge.presentation.currencies

import me.koallann.psafechallenge.R
import me.koallann.psafechallenge.databinding.ItemCurrencyBinding
import me.koallann.psafechallenge.domain.Currency
import me.koallann.psafechallenge.presentation.shared.extensions.format
import me.koallann.psafechallenge.presentation.shared.recyclerview.ViewHolder

class CurrencyViewHolder(
    private val binding: ItemCurrencyBinding,
    private val presenter: CurrenciesPresenter
) : ViewHolder<Currency>(binding.root) {

    override fun onBind(item: Currency) {
        binding.also {
            it.presenter = presenter
            it.position = adapterPosition + 1
            it.currency = item
            it.price.text = binding.root.context.getString(
                R.string.label_currency_price,
                item.symbol,
                item.quote.price.format(item.quote.iso4217Code, 2)
            )
            it.executePendingBindings()
        }
    }

}
