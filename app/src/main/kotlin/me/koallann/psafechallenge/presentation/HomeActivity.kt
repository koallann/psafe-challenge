package me.koallann.psafechallenge.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import me.koallann.psafechallenge.R
import me.koallann.psafechallenge.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {

    private val binding: ActivityHomeBinding by lazy {
        DataBindingUtil.setContentView<ActivityHomeBinding>(this, R.layout.activity_home)
    }
    private val navController: NavController by lazy {
        val navHost = supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment
        navHost.navController
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.run { }
        setupLayout()
    }

    override fun onSupportNavigateUp(): Boolean = navController.navigateUp()

    private fun setupLayout() {
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

}
