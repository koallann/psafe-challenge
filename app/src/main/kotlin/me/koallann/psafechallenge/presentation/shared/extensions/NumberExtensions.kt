package me.koallann.psafechallenge.presentation.shared.extensions

import java.text.NumberFormat

fun Long.format(): String {
    val fmt = NumberFormat.getInstance()
    return fmt.format(this)
}

fun Float.format(maxFractionDigits: Int = 2): String {
    val fmt = NumberFormat.getInstance().apply {
        maximumFractionDigits = maxFractionDigits
    }
    return fmt.format(this.toDouble())
}

fun Double.format(currencyCode: String, maxFractionDigits: Int = 4): String {
    return try {
        val fmt = NumberFormat.getCurrencyInstance().apply {
            currency = java.util.Currency.getInstance(currencyCode)
            maximumFractionDigits = maxFractionDigits
        }
        fmt.format(this)
    } catch (e: Exception) {
        "-"
    }
}
