package me.koallann.psafechallenge.presentation.currencies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import me.koallann.psafechallenge.BuildConfig
import me.koallann.psafechallenge.R
import me.koallann.psafechallenge.data.currency.CurrencyRepositoryImpl
import me.koallann.psafechallenge.data_db.currency.CurrencyDaoClient
import me.koallann.psafechallenge.data_network.currency.CurrencyWebService
import me.koallann.psafechallenge.databinding.FragmentCurrenciesBinding
import me.koallann.psafechallenge.databinding.ItemCurrencyBinding
import me.koallann.psafechallenge.domain.Currency
import me.koallann.psafechallenge.presentation.base.BaseFragment
import me.koallann.psafechallenge.presentation.shared.coroutines.AppDispatcherProvider
import me.koallann.psafechallenge.presentation.shared.recyclerview.AutoRecyclerAdapter
import me.koallann.psafechallenge.workers.CurrencyWorker
import java.util.*

class CurrenciesFragment : BaseFragment(), CurrenciesView {

    companion object {
        private const val BUNDLE_CURRENCIES = "currencies"
    }

    private val binding: FragmentCurrenciesBinding by lazy {
        FragmentCurrenciesBinding.inflate(layoutInflater)
    }
    private val currenciesAdapter: AutoRecyclerAdapter<Currency, CurrencyViewHolder> by lazy {
        AutoRecyclerAdapter { layoutInflater, parent, _ ->
            CurrencyViewHolder(
                ItemCurrencyBinding.inflate(layoutInflater, parent, false),
                presenter
            )
        }
    }

    private val currencyCode: String by lazy {
        java.util.Currency.getInstance(Locale.getDefault()).currencyCode
    }
    private val presenter: CurrenciesPresenter by lazy {
        CurrenciesPresenter(
            CurrencyRepositoryImpl(
                CurrencyDaoClient(requireContext()),
                CurrencyWebService(
                    getString(R.string.currencies_base_url),
                    BuildConfig.API_KEY,
                    currencyCode
                )
            ),
            AppDispatcherProvider()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupLayout()
        presenter.attachView(this)

        val currencies = savedInstanceState?.getParcelableArrayList<Currency>(BUNDLE_CURRENCIES)
        currencies?.let { showCurrencies(it) } ?: presenter.onLoadCurrencies()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (currenciesAdapter.size() > 0) {
            outState.putParcelableArrayList(
                BUNDLE_CURRENCIES,
                currenciesAdapter.getAll() as ArrayList<Currency>
            )
        }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    private fun setupLayout() {
        binding.currencies.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
            adapter = currenciesAdapter
        }
    }

    override fun showLoading() {
        binding.progress.visibility = View.VISIBLE
    }

    override fun dismissLoading() {
        binding.progress.visibility = View.GONE
    }

    override fun showCurrencies(currencies: List<Currency>) {
        currenciesAdapter.clear()
        currenciesAdapter.addAll(currencies)
        CurrencyWorker.schedule(requireContext())
    }

    override fun navigateToDetails(currency: Currency) {
        val args = Bundle().apply { putParcelable("currency", currency) }
        findNavController().navigate(R.id.action_currencies_to_currencyDetails, args)
    }

}
