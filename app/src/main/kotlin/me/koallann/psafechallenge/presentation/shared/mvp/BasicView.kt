package me.koallann.psafechallenge.presentation.shared.mvp

import androidx.annotation.StringRes

interface BasicView {

    fun showMessage(message: String)

    fun showMessage(@StringRes messageRes: Int)

}
