package me.koallann.psafechallenge.data.currency

import me.koallann.psafechallenge.domain.Currency

class CurrencyRepositoryImpl(
    private val localDataSource: CurrencyLocalDataSource,
    private val remoteDataSource: CurrencyRemoteDataSource
) : CurrencyRepository {

    override suspend fun getCurrencies(): List<Currency> {
        var currencies = localDataSource.getCurrencies()

        return if (currencies.isEmpty()) {
            currencies = remoteDataSource.getCurrencies()
            localDataSource.saveCurrencies(currencies)
            currencies
        } else {
            currencies
        }
    }

    override suspend fun refreshCurrencies() {
        localDataSource.deleteCurrencies()
        val currencies = remoteDataSource.getCurrencies()
        localDataSource.saveCurrencies(currencies)
    }

}
