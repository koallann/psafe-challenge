package me.koallann.psafechallenge.data.currency

import me.koallann.psafechallenge.domain.Currency

interface CurrencyRemoteDataSource {

    suspend fun getCurrencies(): List<Currency>

}
