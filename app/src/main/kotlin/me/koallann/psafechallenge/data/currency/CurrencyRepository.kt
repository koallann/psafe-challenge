package me.koallann.psafechallenge.data.currency

import me.koallann.psafechallenge.domain.Currency

interface CurrencyRepository {

    suspend fun getCurrencies(): List<Currency>

    suspend fun refreshCurrencies()

}
