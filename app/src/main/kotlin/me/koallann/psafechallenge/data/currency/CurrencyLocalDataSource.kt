package me.koallann.psafechallenge.data.currency

import me.koallann.psafechallenge.domain.Currency

interface CurrencyLocalDataSource {

    suspend fun getCurrencies(): List<Currency>

    suspend fun saveCurrencies(currencies: List<Currency>)

    suspend fun deleteCurrencies()

}
