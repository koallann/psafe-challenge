package me.koallann.psafechallenge.data_db.currency

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface CurrencyDao {

    @Query("SELECT * FROM currency")
    fun findAll(): List<CurrencyEntity>

    @Insert
    fun insertAll(currencies: List<CurrencyEntity>)

    @Query("DELETE FROM currency")
    fun deleteAll()

}
