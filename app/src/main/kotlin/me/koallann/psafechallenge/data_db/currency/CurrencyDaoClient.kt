package me.koallann.psafechallenge.data_db.currency

import android.content.Context
import me.koallann.psafechallenge.data.currency.CurrencyLocalDataSource
import me.koallann.psafechallenge.data_db.AppDatabase
import me.koallann.psafechallenge.domain.Currency

class CurrencyDaoClient(context: Context) : CurrencyLocalDataSource {

    private val dao: CurrencyDao = AppDatabase.getInstance(context).getCurrencyDao()

    override suspend fun getCurrencies(): List<Currency> {
        val entities = dao.findAll()
        return entities.map { it.toDomain() }
    }

    override suspend fun saveCurrencies(currencies: List<Currency>) {
        val entities = currencies.map {
            CurrencyEntity(
                name = it.name,
                symbol = it.symbol,
                circulatingSupply = it.circulatingSupply,
                quote = CurrencyEntity.Quote(
                    it.quote.iso4217Code,
                    it.quote.marketCap,
                    it.quote.volume24h,
                    it.quote.price,
                    it.quote.pricePercent24h
                )
            )
        }
        dao.insertAll(entities)
    }

    override suspend fun deleteCurrencies() {
        dao.deleteAll()
    }

}
