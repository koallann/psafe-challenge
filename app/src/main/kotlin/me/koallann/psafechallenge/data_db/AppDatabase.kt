package me.koallann.psafechallenge.data_db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import me.koallann.psafechallenge.data_db.currency.CurrencyDao
import me.koallann.psafechallenge.data_db.currency.CurrencyEntity

@Database(version = 1, entities = [CurrencyEntity::class])
abstract class AppDatabase : RoomDatabase() {

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase = synchronized(this) {
            return INSTANCE ?: synchronized(this) {
                buildDatabase(context).also { INSTANCE = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "cryptocurrency.db"
            ).build()
        }
    }

    abstract fun getCurrencyDao(): CurrencyDao

}
