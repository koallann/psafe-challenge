package me.koallann.psafechallenge.data_db.currency

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import me.koallann.psafechallenge.domain.Currency

@Entity(tableName = "currency")
class CurrencyEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val name: String,
    val symbol: String,
    @ColumnInfo(name = "circulating_supply")
    val circulatingSupply: Long,
    @Embedded(prefix = "quote_")
    val quote: Quote
) {

    fun toDomain(): Currency = Currency(name, symbol, circulatingSupply, quote.toDomain())

    class Quote(
        @ColumnInfo(name = "iso_4217_code")
        val iso4217Code: String,
        @ColumnInfo(name = "market_cap")
        val marketCap: Double,
        @ColumnInfo(name = "volume_24h")
        val volume24h: Double = 0.0,
        val price: Double,
        @ColumnInfo(name = "volume_percent_24h")
        val pricePercent24h: Float = 0.0f
    ) {
        fun toDomain(): Currency.Quote = Currency.Quote(
            iso4217Code,
            marketCap,
            volume24h,
            price,
            pricePercent24h
        )
    }

}
