package me.koallann.psafechallenge.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Currency(
    var name: String = "",
    var symbol: String = "",
    var circulatingSupply: Long = 0L,
    var quote: Quote = Quote()
) : Parcelable {

    @Parcelize
    data class Quote(
        var iso4217Code: String = "",
        var marketCap: Double = 0.0,
        var volume24h: Double = 0.0,
        var price: Double = 0.0,
        var pricePercent24h: Float = 0.0f
    ) : Parcelable

}
